<?php

namespace Cetria\Enums;

class HttpStatus
{
    const OK = 200;
    const CREATED = 201;
    const ACCEPTED = 202;
    const DELETED = 202;
    const NO_CONTENT = 204;
    const UNAUTHORIZED = 401;
    const FORBIDDEN = 403;
    const NOT_FOUND = 404;
    const NOT_ALLOWED = 405;
    const CONFLICT = 409;
    const UNPROCESSABLE_ENTITY = 422;
    const BAD_INPUT_PARAMS = 422;
    const NOT_IMPLEMENTED = 501;
}
